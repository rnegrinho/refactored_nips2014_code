
import numpy as np


def random_model(random_generator, d, k, rg_init=None):
    np.random.seed(rg_init)
    nnz_x = random_generator(size=k)
    np.random.seed()

    nnz_x -= nnz_x.mean()
    nnz_x.sort()

    x = np.zeros(d)
    x[0:k] = nnz_x
    x /= np.linalg.norm(x)

    return x

def random_dataset(x, N, noise_std, dataset_rg_init=None, noise_rg_init=None):
    d = len(x)

    np.random.seed(dataset_rg_init)
    A = np.random.normal(size=(N, d)) * (1 / np.sqrt(d))
    np.random.seed()

    b = A.dot(x)

    np.random.seed(noise_rg_init)
    noise = noise_std * np.random.normal(size=N)
    np.random.seed()

    #import ipdb
    #ipdb.set_trace()

    b += noise

    return (A, b)


if __name__ == '__main__':
    np.set_printoptions(threshold=1)

    rg = np.random.normal
    d = 100
    k = 10

    x = random_model(rg, d, k)
    print x

    N = 10
    noise_std = 0.01
    A, b = random_dataset(x, N, noise_std)
    print A, b


