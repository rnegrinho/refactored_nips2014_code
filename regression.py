
import quadratic_cost as qc
import frank_wolfe as fw


def quadratic_regression(x0, A, b, minimum_atom_function, max_dgap, max_delta, max_nsteps):

    cost_function = qc.quadratic_cost_factory(A, b)
    gradient_function = qc.quadratic_gradient_factory(A, b)

    res = fw.frank_wolfe_optimizer(x0, cost_function, gradient_function,
        minimum_atom_function, max_nsteps, max_dgap, max_delta)

    return res

# uses a sort of generator object to do continuation
def quadratic_regression_continuation(x0, A, b, minimum_atom_generator,
    max_dgap, max_delta, max_nsteps):

    res = []
    xwarm = x0

    while not minimum_atom_generator.is_empty():
        minimum_atom_function = minimum_atom_generator.get_minimum_atom_function()
        res_item = quadratic_regression(xwarm, A, b, minimum_atom_function,
            max_dgap, max_delta, max_nsteps)

        res.append(res_item)
        xwarm = res_item['xk']
        minimum_atom_generator.next(xwarm)

    return res




