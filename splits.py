
import numpy as np


# split generator
def split_generator(A, b, min_samples, nsplits):
    N = len(b)

    splits = np.linspace(min_samples, N, nsplits).astype(int)

    for n in splits:
        yield (A[:n], b[:n])

