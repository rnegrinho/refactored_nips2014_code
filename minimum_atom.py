
import numpy as np


def l2_ball_minimum_atom_function(grad_x):
    norm_grad_x = np.linalg.norm(grad_x)

    # avoid dividing by very small numbers
    if norm_grad_x < 1e-32:
        norm_grad_x = 1.0

    return -grad_x / np.linalg.norm(grad_x)

def l1_ball_minimum_atom_function(grad_x):
    abs_grad_x = np.abs(grad_x)
    imax_abs_grad_x = np.argmax(abs_grad_x)

    atom = np.zeros_like(grad_x)
    atom[imax_abs_grad_x] = -np.sign(grad_x[imax_abs_grad_x])

    return atom

def hyperoctahedral_seed_ball_minimum_atom_factory(seed):
    sorted_abs_seed = np.sort(np.abs(seed))

    def hyperoctahedral_seed_ball_minimum_atom_function(grad_x):
        abs_grad_x = np.abs(grad_x)
        isorted_abs_grad_x = np.argsort(abs_grad_x)
        inv_isorted_abs_grad_x = np.argsort(isorted_abs_grad_x)

        abs_atom = sorted_abs_seed[inv_isorted_abs_grad_x]
        atom = -np.sign(grad_x) * abs_atom
        return atom

    return hyperoctahedral_seed_ball_minimum_atom_function

def symmetric_seed_ball_minimum_atom_factory(seed):
    sorted_seed = np.sort(seed)

    def symmetric_seed_ball_minimum_atom_function(grad_x):
        isorted_grad_x = np.argsort(grad_x)[::-1]
        inv_isorted_grad_x = np.argsort(isorted_grad_x)

        atom = sorted_seed[inv_isorted_grad_x]
        return atom

    return symmetric_seed_ball_minimum_atom_function

