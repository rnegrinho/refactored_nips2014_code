
import numpy as np
import matplotlib.pyplot as plt
import os
import load_results
import itertools
import ipdb

# should have used 10 samples to start with and always go up to the dimension

### NOTE be careful with the split sizes, why didn't I allways used the same split size
# who knows, can I guarantee that the split size is constant when we fix the
# number of nonzeros

# create a script that generates all the graphs
# NOTE I will probably remove the script that generated all the alpha crap
# NOTE I could load the data at the global level, it would be easier
# and I'm not changing it, anyways.
# NOTE do it inside the main, there is no point having functions that have
# access to it before doing
# the functions would have to take the tensor list

# NOTE the split sides depend on the number of nonzeros

# change the the name of the thing to be more approapriate.
# when it finishes see what it gives.

# this code is somewhat bad. can I do something about it?
# NOTE some of the functionalities here can be put in a separate file.
# the summarize runs part is not uniform. It can be better done.

#### NOTE it is necessary to do something with the figure
# to generate the plots
# NOTE these next three functions are useful to probe the results

# NOTE rerun the seed script with just 3 runs for checking things

# note that this is going to be applied to all
# takes all the regs and returns an index
def get_indexed_results(tensor_dict, tag, to_index):
    tensor = tensor_dict[tag]

    best_index = lambda tensor: [[to_index(regs)
        for regs in splits]
        for splits in tensor]

    index_tensor = lambda tensor, index: np.array([[regs[reg_index]
        for reg_index, regs in zip(splits_index, splits)]
        for splits_index, splits in zip(index, tensor)])

    index = best_index(tensor_dict[tag])
    indexed_results_dict = {tag: index_tensor(tensor_dict[tag], index)
        for tag in tensor_dict}

    return indexed_results_dict


# use a tensor list form of this
def get_validation_best(tensor_dict):
    return get_indexed_results(tensor_dict, 'val_errors', np.argmin)


# the indexing should probably be done here
# NOTE careful about coming up with the split sizes.
# they are not the same irrespective of the problem (they depend on nnz, I guess)
# in the alpha part, they may depend on more strange things
def plot_performance_vs_splits(split_sizes, tag, tensor_list, label_list, out_path=None):

    for t, label in zip(tensor_list, label_list):
        ival_t = get_validation_best(t)
        # axis 0 is across runs
        m = np.mean(ival_t[tag], axis=0)
        std = np.std(ival_t[tag], axis=0)
        plt.errorbar(split_sizes, m, std, marker='s', label=label)

    plt.legend(loc=0)
    plt.ylabel(tag)

    if out_path is None:
        plt.show()
    else:
        plt.savefig(out_path)

    plt.close()


def plot_tagy_vs_tagx(tagy, tagx, irun, isplit, tensor_list, label_list, out_path=None):

    for t, label in zip(tensor_list, label_list):
        tx = t[tagx][irun][isplit]
        ty = t[tagy][irun][isplit]
        plt.plot(tx, ty, marker='s', label=label)

    plt.legend(loc=0)
    plt.xlabel(tagx)
    plt.ylabel(tagy)

    if out_path is None:
        plt.show()
    else:
        plt.savefig(out_path)

    plt.close()

#### this part is not extensible, as it is related to the graphs appearing on
# the nips paper
def generate_fig4_plots():
    out = 'out_final_more_noise_more_data/'
    out = 'out_final_more_consts/'
    out = 'out_final_noise_ratio/'
    out = 'out_final_fixed_noise/'
    noise_str = '3e-1'
    n = 20

    for (method, nnz) in itertools.product(['l1', 'l2'], ['150', '250', '400', '500']):
        print (method, nnz)

        suffix = '_d500_nnz' + nnz + '_unif_noise' + noise_str

        # this part has to be changed
        split_sizes, baseline = load_results.load_summary(out + method + suffix)
        s2, sym = load_results.load_summary(out + 'sym' + suffix)
        s3, oct = load_results.load_summary(out + 'oct' + suffix)

        assert np.all(split_sizes == s2) and np.all(split_sizes == s3), \
            "The split size values don't match."

        # this part can be done using a list
        ival_tensor_list = [get_validation_best(baseline),
                            get_validation_best(sym),
                            get_validation_best(oct)]

        marker_list = ['bo-', 'gs-', 'r^-']
        markersize_list = [10.0, 10.0, 12.0]
        label_list = ['$\\ell_%d$' % int(method[-1]), 'perm', 'sigperm']

        for tensor, marker, markersize, label in zip(ival_tensor_list, marker_list, markersize_list, label_list):
            m = np.mean(tensor['recons_errors'], axis=0)
            std = np.std(tensor['recons_errors'], axis=0)

            plt.errorbar(split_sizes[0:n], m[0:n], std[0:n], fmt=marker, label=label,
                #markeredgecolor=marker[0],
                markerfacecolor='none', markeredgecolor=marker[0], markeredgewidth=1.2,
                markersize=markersize)

        plt.xlabel('Sample Size $n$', fontsize=18)
        #plt.ylabel('Reconstruction Error $\\frac{|| w - \hat{w} ||}{|| \hat{w} ||} $', fontsize=18)
        plt.ylabel('Reconstruction Error $|| w - \hat{w} || \ || \hat{w} || $', fontsize=18)
        # this has to be set according to the desired xlimit
        plt.ylim([0.0, 1.1])
        #plt.xlim([0, 500])

        plt.legend(loc=0, prop={'size':17})
        ax = plt.gca()
        ax.set_aspect(500)
        #ipdb.set_trace()
        plt.savefig('plots/' + method + '_' + nnz + '.svg', bbox_inches='tight')
        plt.close()


def generate_fig5_plots():
    seed_folder = 'out_fig5_noise_test/'
    noise_str = '5e-5'

    # the outer indexing is done exactly using the following order
    #nnz_list = ['150', '250', '400', '500']
    nnz_list = ['250']
    alpha_list = ['0.0', '0.1', '0.3', '0.5', '0.9', '1.0']
    #alpha_list = ['0.0']

    # seed information (sym, oct, sym_cont, oct_cont)
    seed_method_list = ['sym', 'oct', 'sym_cont', 'oct_cont']
    seed_path_tensor = generate_out_alpha_paths(nnz_list, seed_method_list,
        alpha_list, noise_str, seed_folder)
    seed_split_tensor, seed_data_tensor = load_paths(seed_path_tensor)

    # baseline information (l1, l2)
    baseline_method_list = ['l1', 'l2']
    baseline_path_tensor = generate_out_final_paths(nnz_list,
        baseline_method_list, noise_str, seed_folder)
    baseline_split_tensor, baseline_data_tensor = load_paths(baseline_path_tensor)

    # labels used for the information
    seed_label_tensor = generate_labels(['nnz', '', 'a'], [nnz_list, seed_method_list, alpha_list])
    baseline_label_tensor = generate_labels(['nnz', ''], [nnz_list, baseline_method_list])

    # indexing of all the relevant dimensions to plot a one dimensional slice
    # of the results tensor
    # NOTE the baseline does not has alpha
    #innz = 0
    imethod = 0
    ialpha = 0
    irun = 0
    isplit = 0
    tagx = 'l2_norms'
    tagy = 'test_errors'

    # do some sort of labelling
    for innz in xrange(len(nnz_list)):
        #print baseline_split_tensor[innz, :, 0]

        #ipdb.set_trace()
        seed_tensor_list = seed_data_tensor[innz, :, ialpha]
        baseline_tensor_list = baseline_data_tensor[innz]

        seed_label_list = seed_label_tensor[innz, :, ialpha]
        baseline_label_list = baseline_label_tensor[innz]

        #print seed_label_list.shape
        #print baseline_label_list.shape
        #ipdb.set_trace()

        tensor_list = np.concatenate([baseline_tensor_list, seed_tensor_list])
        label_list = np.concatenate([baseline_label_list, seed_label_list])

        marker_list = ['b^-', 'go-', 'rs-', 'cs-', 'ms-', 'ys-']
        #label_list = ['$\\ell_%d$' % int(method[-1]), 'perm', 'sigperm']

        for t, l, m in zip(tensor_list, label_list, marker_list):
            # for each tensor it should be easy to get the information for
            # the validation score

            #ipdb.set_trace()
            #print t[tagx][irun, isplit], l

            plt.plot(t[tagx][irun, isplit], t[tagy][irun, isplit], m, label=l)


        plt.xlabel('$\\ell_1$', fontsize=16)
        plt.ylabel('Test Error', fontsize=16)
        plt.legend(loc=0, prop={'size':15})
        plt.show()

        # this has to be set according to the desired xlimit
        #plt.savefig('plots/' + method + '_' + nnz + '.svg')
        #plt.close()


def generate_fig5_plots_new():
    seed_folder = 'out_fig5_noise_testing5/'
    noise_str = '3e-1'
    rg_str = 'unif'

    # the outer indexing is done exactly using the following order
    #nnz_list = ['150', '250', '400', '500']
    nnz_list = ['250']
    alpha_list = ['0.0']

    # seed information (sym, oct, sym_cont, oct_cont)
    seed_method_list = ['sym_cont', 'oct_cont']
    seed_path_tensor = generate_out_alpha_paths(nnz_list, seed_method_list,
        alpha_list, noise_str, seed_folder, rg_str=rg_str)
    seed_split_tensor, seed_data_tensor = load_paths(seed_path_tensor)

    # baseline information (l1, l2)
    baseline_method_list = ['l1', 'l2']
    baseline_path_tensor = generate_out_final_paths(nnz_list,
        baseline_method_list, noise_str, seed_folder, rg_str=rg_str)
    #ipdb.set_trace()
    baseline_split_tensor, baseline_data_tensor = load_paths(baseline_path_tensor)

    # labels used for the information
    seed_label_tensor = generate_labels(['nnz', '', 'a'], [nnz_list, seed_method_list, alpha_list])
    baseline_label_tensor = generate_labels(['nnz', ''], [nnz_list, baseline_method_list])

    # indexing of all the relevant dimensions to plot a one dimensional slice
    # of the results tensor
    # NOTE the baseline does not has alpha
    #innz = 0
    ialpha = 0
    isplit = 0
    tagx = 'l2_norms'
    tagy = 'train_errors'

    # this just makes sense for the
    irun = 0
    nruns = 5
    # first plot
    # do some sort of labelling
    # one run and one split, except for the other
    #marker_list = ['b^-', 'go-']['rs-', 'cs-', 'ms-', 'ys-']

    def aux_plot(tagy, xlabel, ylabel, flag=False):

        for innz in xrange(len(nnz_list)):
            #print baseline_split_tensor[innz, :, 0]

            # for the baseline
            baseline_tensor_list = baseline_data_tensor[innz]
            baseline_label_list = baseline_label_tensor[innz]

            for t, l, m in zip(baseline_tensor_list, ['$\\ell_1$', '$\\ell_2$'], ['b^-', 'go-']):
                plt.plot(t[tagx][0, isplit], t[tagy][0, isplit], m, label=l,
                    markerfacecolor='none', markeredgecolor=m[0], markeredgewidth=1.2)

                if flag:
                    # plot the best, probably with no marker
                    ind = np.argmin(t['val_errors'][0, isplit])
                    best_val = t[tagy][0, isplit, ind]
                    plt.axhline(best_val, color=m[0])


            # for the seed
            seed_tensor_list = seed_data_tensor[innz, :, ialpha]
            seed_label_list = seed_label_tensor[innz, :, ialpha]

            # NOTE single nnz, single method, single alpha
            for t, l, m in zip(seed_tensor_list, ['perm', 'sigperm'], ['rs-', 'cp-']):
                best_irun = None
                best_ind = None
                best_valid = np.inf
                for i in xrange(nruns):
                    if i == 0:
                        plt.plot(t[tagx][i, isplit], t[tagy][i, isplit], m, label=l,
                            markerfacecolor='none', markeredgecolor=m[0], markeredgewidth=1.2)
                    else:
                        plt.plot(t[tagx][i, isplit], t[tagy][i, isplit], m,
                            markerfacecolor='none', markeredgecolor=m[0], markeredgewidth=1.2)

                    if flag:
                        # plot the best, probably with no marker
                        ind = np.argmin(t['val_errors'][i, isplit])
                        val = t['val_errors'][i, isplit, ind]

                        if val < best_valid:
                            best_valid = val
                            best_ind = ind
                            best_irun = i

                if flag:
                    best_val = t[tagy][best_irun, isplit, best_ind]
                    plt.axhline(best_val, color=m[0])


            plt.xlabel(xlabel, fontsize=18)
            plt.ylabel(ylabel, fontsize=18)
            plt.legend(loc=0, prop={'size':17})
            plt.tight_layout()
            #plt.axis('tight')


    aux_plot('train_errors', '$\\ell_2$-norm of the solution', 'Training Error')
    fig = plt.gcf()
    fig.set_size_inches(10.5,5.5)
    ax = plt.gca()
    #ax.set_aspect(500)
    plt.savefig('plots/train.svg', bbox_inches='tight')
    plt.close()
    #plt.show()
    aux_plot('test_errors', '$\\ell_2$-norm of the solution', 'Test Error', True)
    #ax = plt.gca()

    #fig.set_size_inches(18.5,10.5)
    #fig.savefig('test2png.png',dpi=100)

    fig = plt.gcf()
    fig.set_size_inches(10.5,5.5)
    plt.savefig('plots/test.svg', bbox_inches='tight')
    #plt.show()
    plt.close()

    # NOTE I have to plot both the train and test


        # this has to be set according to the desired xlimit
        #plt.savefig('plots/' + method + '_' + nnz + '.svg')
        #plt.close()

# I can do better path generation, but that is not something that I want to
# do now

def generate_paths(id_values_list):
    pass


def prefix_paths(path_tensor, prefix_path):
    s = path_tensor.shape
    prefixed_path_list = [os.path.join(prefix_path, p) for p in path_tensor.ravel()]
    prefixed_path_tensor = np.array(prefixed_path_list).reshape(s)

    return prefixed_path_tensor


# ''method, d'500' generator noise and so forth
# do a method to do this in some better way
def generate_out_final_paths(nnz_list, method_list, noise_str, folderpath, rg_str='unif'):
    path_tensor = np.array([[os.path.join(folderpath,
                                          method +
                                          '_d500_nnz' + nnz +
                                           '_' + rg_str +
                                          '_noise' + noise_str)
        for method in method_list]
        for nnz in nnz_list])

    return path_tensor


def generate_out_alpha_paths(nnz_list, method_list, alpha_list, noise_str, folderpath, rg_str='unif'):
    path_tensor = np.array([[[os.path.join(folderpath,
                                           '' + method +
                                           '_alpha' + alpha +
                                           '_d' + '500' +
                                           '_nnz' + nnz +
                                           '_' + rg_str +
                                           '_noise' + noise_str)
        for alpha in alpha_list]
        for method in method_list]
        for nnz in nnz_list])

    return path_tensor


# this does not do exactly what you want. it has to unpack the split sizes first
def load_paths(path_tensor):
    s = path_tensor.shape
    pairs_list = map(load_results.load_summary, path_tensor.ravel())

    #ipdb.set_trace()

    split_sizes_tensor, data_tensor = zip(*pairs_list)

    nsplits = len(split_sizes_tensor[0])
    split_sizes_tensor = np.array(split_sizes_tensor).reshape(s + (nsplits,))
    data_tensor = np.array(data_tensor).reshape(s)

    return (split_sizes_tensor, data_tensor)


def generate_labels(id_list, value_list):

    assert len(id_list) == len(value_list), 'Lists must have the same length.'

    label_list = list(itertools.product(*value_list))
    label_list = ['_'.join([''.join(p) for p in zip(id_list, t)]) for t in label_list]

    s = tuple([len(l) for l in value_list])
    label_tensor = np.array(label_list).reshape(s)

    return label_tensor

# this is the data structure that is loaded from there
# [innz, imethod, ialpha] ->
# (['recons_errors', 'train_errors', 'val_errors', 'test_errors', 'l1_norms', 'l2_norms'] ->
# [irun, isplit, ireg]

if __name__ == '__main__':
    # value list is a list of lists

    #nnz_list = ['150', '250', '400', '500']
    #alpha_list = ['0.0', '0.1', '0.3', '0.5', '0.9', '1.0']

    ## final experiments
    #final_method_list = ['l1', 'l2', 'sym', 'oct', 'sym_cont', 'oct_cont']
    #final_path_tensor = generate_out_final_paths(nnz_list, final_method_list,
        #'1e-5', 'out_final')
    #final_split_tensor, final_data_tensor = load_paths(final_path_tensor)
    #final_label_tensor = generate_labels(['nnz', ''], [nnz_list, final_method_list])

    ## alpha experiments
    #alpha_method_list = ['sym', 'oct', 'sym_cont', 'oct_cont']
    #alpha_path_tensor = generate_out_alpha_paths(nnz_list, alpha_method_list,
        #alpha_list, '1e-5', 'out_alpha')
    #alpha_split_tensor, alpha_data_tensor = load_paths(alpha_path_tensor)
    #alpha_label_tensor = generate_labels(['nnz', '', 'a'], [nnz_list, alpha_method_list, alpha_list])

    ## seed experiments
    ### NOTE NOTE
    #seed_folder = 'out_seed_fixed_more_consts'
    #seed_folder = 'out_seed_fixed'

    #seed_method_list = ['sym', 'oct', 'sym_cont', 'oct_cont']
    #seed_path_tensor = generate_out_alpha_paths(nnz_list, seed_method_list,
        #alpha_list, '3e-5', seed_folder)
    #seed_split_tensor, seed_data_tensor = load_paths(seed_path_tensor)
    #seed_label_tensor = generate_labels(['nnz', '', 'a'], [nnz_list, seed_method_list, alpha_list])

    ## seed baselines
    ## this can be done with some things
    #baseline_seed_method_list = ['l1', 'l2']
    #baseline_seed_path_tensor = generate_out_final_paths(nnz_list,
        #baseline_seed_method_list, '3e-5', seed_folder)
    #baseline_seed_split_tensor, baseline_seed_data_tensor = load_paths(baseline_seed_path_tensor)
    #baseline_seed_label_tensor = generate_labels(['nnz', ''], [nnz_list, baseline_seed_method_list])

    ## plot the alpha experiments
    #irun = 0; isplit = 3; innz = 2; imethod = 1; ialpha = 5; tag = 'recons_errors'
    ## example command to print stuff
    #ts = np.concatenate([baseline_seed_data_tensor[innz], seed_data_tensor[innz, :, ialpha].ravel()])
    #ls = np.concatenate([baseline_seed_label_tensor[innz], seed_label_tensor[innz, :, ialpha].ravel()])

    ##plot_tagy_vs_tagx(tag, 'l1_norms', irun, isplit, ts, ls)
    ##plot_performance_vs_splits(seed_split_tensor[innz, 0, 0], tag, ts, ls)

    ### NOTE I'm here. all the remaining commented code has to be reviewed
    #generate_fig4_plots()
    generate_fig5_plots_new()
    #### NOTE NOTE NOTE
    #generate_fig5_plots()

    #### NOTE, this has a lot of irrelevant code


    # do some code to plot things as they should be

    #plot_tagy_vs_tagx('test_errors', 'l1_norms',
        #irun, isplit, alpha_data_tensor[innz, imethod, :].ravel(), alpha_label_tensor[innz, imethod, :].ravel())

    # they should come in tensor form

# it could pass the noise, but for now, I will not do so
# I can pass some noise str and some
# do it later, for now, there s no point

# NOTE somethings here are hard coded
    # there is a single dimension to use (500)


    ## NOTE do the tensor manipulations in the code
    ##generate_fig4_plots()


    # output all the possible experiments to plots


    ## little script for experiments
    ## NOTE
    #def plot_final_dump(tagy, nruns):

        #for irun, innz, isplit in itertools.product(xrange(nruns), xrange(4), xrange(20)):
            #fpath = 'plots/exp/r' + str(irun) + '_' + tagy + \
                    #'_nnz' + str(nnz_list[innz]) + \
                    #'_s' + str(split_sizes[isplit]) + '.svg'

            #plot_tagy_vs_tagx(tagy, 'l1_norms', irun, isplit,
                #method_tensor_list[innz], method_list, fpath)
    ###### I have to find a better way to deal with the plots

    #alpha_list = [0.0, 0.1, 0.3, 0.5, 0.9, 1.0]
    #nnz_list = [150, 250, 400, 500]
    #method_list = ['sym', 'oct', 'sym_cont', 'oct_cont']





