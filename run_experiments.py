
# NOTE maybe normalize with respect to the l1 norm instead of the l2 norm
# NOTE the seed use case is not completely clear. If no arguments are specified
# choose the output to the log
# he simply uses the original model to parametrize the regularization ball
# this use case should be clearer from the argument list

import argparse
import datasets
import minimum_atom
import continuation
import regression
import splits
import numpy as np
import pickle
import pprint


def get_argument_parser():
    parser = argparse.ArgumentParser()

    # model options
    model_group = parser.add_argument_group('model options')
    model_group.add_argument('model_d',
        help='dimension of the model',
        type=int)

    model_group.add_argument('model_nnz',
        help='number of nonzeros of the model',
        type=int)

    model_group.add_argument('model_rg',
        help='model random generator',
        choices=['const', 'unif', 'gauss', 'lap'])

    model_group.add_argument('--model_rg_init',
        help='initialization of the model random generator',
        type=int)

    # dataset options
    dataset_group = parser.add_argument_group('dataset options')
    dataset_group.add_argument('N_train',
        help='number of training samples',
        type=int)

    dataset_group.add_argument('N_val',
        help='number of validation samples',
        type=int)

    dataset_group.add_argument('N_test',
        help='number of test samples',
        type=int)

    dataset_group.add_argument('noise_std',
        help='standard deviation of the added gaussian noise',
        type=float)

    dataset_group.add_argument('--dataset_rg_init',
        help='initialization of the dataset random generator',
        type=int)

    dataset_group.add_argument('--noise_rg_init',
        help='initialization of the noise random generator',
        type=int)

    # simulation options
    simulation_group = parser.add_argument_group('simulation options')
    simulation_group.add_argument('min_samples',
        help='minimum number of samples to use',
        type=int)

    simulation_group.add_argument('nsplits',
        help='number of splits for which we run the experiments',
        type=int)

    simulation_group.add_argument('out_data',
        help='name for the output pickle file with the data',
        type=str)

    simulation_group.add_argument('out_text',
        help='name for the output text file with a summary of the experiment',
        type=str)


    optimization_group = parser.add_argument_group('optimization options')
    optimization_group.add_argument('max_nsteps',
        help='maximum number of steps for the optimizer',
        type=int)

    optimization_group.add_argument('max_dgap',
        help='maximum duality gap for the optimizer',
        type=float)

    optimization_group.add_argument('max_delta',
        help='minimum l2 norm between consecutive updates',
        type=float)

    # estimation options
    estimation_subparsers = parser.add_subparsers()

    parser_regular = estimation_subparsers.add_parser('regular')
    regular_estimation_group = parser_regular.add_argument_group('estimation options')
    regular_estimation_group.add_argument('reg',
        help='regularization to use',
        choices=['l1', 'l2'])

    regular_estimation_group.add_argument('nconsts',
        help='number of regularization constants',
        type=int)

    regular_estimation_group.add_argument('start_const',
        help='initial regularization ball size',
        type=float)

    regular_estimation_group.add_argument('eps',
        help='expansion constant of the regularization ball (>1.0)',
        type=float)


    parser_seed = estimation_subparsers.add_parser('seed')
    seed_estimation_group = parser_seed.add_argument_group('estimation options')
    seed_estimation_group.add_argument('reg',
        help='regularization to use',
        choices=['sym', 'oct'])

    seed_estimation_group.add_argument('nconsts',
        help='number of regularization constants',
        type=int)

    seed_estimation_group.add_argument('start_const',
        help='initial regularization ball size',
        type=float)

    seed_estimation_group.add_argument('eps',
        help='expansion constant of the regularization ball (>1.0)',
        type=float)

    seed_estimation_group.add_argument('alpha',
        help='convex combination of the previous seed and current solution \
            (v_t := alpha * v_(t-1) + (1 - alpha) * w_t)',
        type=float)

    seed_estimation_group.add_argument('--seed_nnz',
        help='number of nonzeros of the seed \
            (if not set, the nnz is equal to the nnz of the model)',
        type=int)

    seed_estimation_group.add_argument('--seed_rg',
        help='seed random generator \
            (if not set, the seed is equal to the model)',
        choices=['const', 'unif', 'gauss', 'lap'])

    seed_estimation_group.add_argument('--seed_rg_init',
        help='initialization of the seed random generator',
        type=int)

    return parser

def get_random_generator(rg_tag):
    if rg_tag == 'const':
        random_generator = lambda size: np.ones(size)
    elif rg_tag == 'unif':
        random_generator = np.random.random
    elif rg_tag == 'gauss':
        random_generator = np.random.normal
    elif rg_tag == 'lap':
        random_generator = np.random.normal
    else:
       raise Exception("Not implemented or unknown random generator.")

    return random_generator

def get_continuator(args):
    # setting up the estimation process
    if args.reg in ['l1', 'l2']:
        if args.reg == 'l1':
            minimum_atom_function = minimum_atom.l1_ball_minimum_atom_function
        else:
            minimum_atom_function = minimum_atom.l2_ball_minimum_atom_function

        continuator = continuation.regular_continuation(minimum_atom_function,
            args.start_const, args.nconsts, args.eps)

    elif args.reg in ['sym', 'oct']:
        if args.reg == 'sym':
            minimum_atom_factory = minimum_atom.symmetric_seed_ball_minimum_atom_factory
        else:
            minimum_atom_factory = minimum_atom.hyperoctahedral_seed_ball_minimum_atom_factory

        if args.seed_rg is not None:
            seed_rg = get_random_generator(args.seed_rg)

            if args.seed_nnz is not None:
                seed_nnz = args.seed_nnz
            else:
                seed_nnz = args.model_nnz

            start_seed = datasets.random_model(seed_rg, args.model_d,
                seed_nnz, args.seed_rg_init)

        else:
            start_seed = x

        continuator = continuation.seed_continuation(minimum_atom_factory,
            start_seed, args.start_const, args.nconsts, args.eps, args.alpha)

    else:
        raise Exception('Unknown regularization method.')

    return continuator


if __name__ == '__main__':
    parser = get_argument_parser()

    args = parser.parse_args()

    model_rg = get_random_generator(args.model_rg)
    # generate the random model
    x = datasets.random_model(model_rg, args.model_d, args.model_nnz, args.model_rg_init)

    # generate a random dataset
    nt, nv, nts = args.N_train, args.N_val, args.N_test
    N = nt + nv + nts
    print "noise:", args.noise_std / np.sqrt(args.model_d)

    A, b = datasets.random_dataset(x, N, args.noise_std / np.sqrt(args.model_d),
        args.dataset_rg_init, args.noise_rg_init)

    A_train, b_train = A[:nt], b[:nt]
    A_val, b_val = A[nt:(nt + nv)], b[nt:(nt + nv)]
    A_test, b_test = A[(nt + nv):], b[(nt + nv):]

    # go over all the training splits and compute the regularization path
    res_splits = []
    for A_train_split, b_train_split in \
        splits.split_generator(A_train, b_train, args.min_samples, args.nsplits):

        x0 = np.zeros(args.model_d)
        continuator = get_continuator(args)
        #import ipdb
        #ipdb.set_trace()

        reg_path = regression.quadratic_regression_continuation(x0, A_train_split,
            b_train_split, continuator, args.max_dgap, args.max_delta, args.max_nsteps)

        output_item = {
            'train_set_split' : (A_train_split, b_train_split),
            'reg_path' : reg_path
            }
        res_splits.append(output_item)

    # keep a dictionary with all the output results
    output = {
        'input_args' : vars(args),
        'x' : x,
        'train_set' : (A_train, b_train),
        'val_set' : (A_val, b_val),
        'test_set' : (A_test, b_test),
        'res_splits' : res_splits
        }

    # output the information to files
    with open(args.out_data + '.pickle', 'wb') as out:
        pickle.dump(output, out)

    with open(args.out_text + '.txt', 'w') as out:
        np.set_printoptions(threshold=25)
        pprint.pprint(output, stream=out)

