
import quadratic_cost
import numpy as np
import cPickle as pickle
import os

# this code is horrible. can I do something about it?

# NOTE some of the functionalities here can be put in a separate file.
# the summarize runs part is not uniform. It can be better done.
# remove the get indexed results from it

def get_run_filenames(run_folderpath):

    run_filename_list = []

    i = 0
    while True:
        fname = 'run' + str(i) + '_data.pickle'
        fpath = os.path.join(run_folderpath, fname)

        if not os.path.isfile(fpath):
            break

        run_filename_list.append(fname)
        i += 1

    return run_filename_list


def summarize_run(run_filepath):

    with open(run_filepath, 'rb') as f:
        data = pickle.load(f)

        # some scoring functions (reconstruction, validation, test, and train errors)
        x = data['x']
        recons_error_function = lambda x_est: np.linalg.norm(x - x_est) / np.linalg.norm(x, 2)

        A_val, b_val = data['val_set']
        val_error_function = quadratic_cost.quadratic_cost_factory(A_val, b_val)

        A_test, b_test = data['test_set']
        test_error_function = quadratic_cost.quadratic_cost_factory(A_test, b_test)

        # NOTE sumarize a run
        info_run = []
        split_sizes = []
        for data_split in data['res_splits']:
            A_train_split, b_train_split = data_split['train_set_split']
            train_error_function = quadratic_cost.quadratic_cost_factory(A_train_split, b_train_split)

            split_sizes.append(len(b_train_split))

            # NOTE summarize a split
            info_split = []
            for data_reg in data_split['reg_path']:
                xreg = data_reg['xk']

                # NOTE summarize a regularization record
                info_reg = {
                    'recons_error' : recons_error_function(xreg),
                    'train_error' : train_error_function(xreg),
                    'val_error' : val_error_function(xreg),
                    'test_error' : test_error_function(xreg),
                    'l1_norm' : np.linalg.norm(xreg, 1),
                    'l2_norm' : np.linalg.norm(xreg, 2)
                    }
                info_split.append(info_reg)
            info_run.append(info_split)

        return (split_sizes, info_run)


def summarize_runs(run_folderpath):

    info_run_list = []
    split_sizes = []

    for fname in get_run_filenames(run_folderpath):
        run_filepath = os.path.join(run_folderpath, fname)
        split_sizes, info_run = summarize_run(run_filepath)
        info_run_list.append(info_run)

    # auxiliary function
    unpack_f = lambda tag: np.array([[[info_reg[tag]
        for info_reg in info_split]
        for info_split in info_run]
        for info_run in info_run_list])

    results_dict = {
        'recons_errors' : unpack_f('recons_error'),
        'train_errors' : unpack_f('train_error'),
        'val_errors' : unpack_f('val_error'),
        'test_errors' : unpack_f('test_error'),
        'l1_norms' : unpack_f('l1_norm'),
        'l2_norms' : unpack_f('l2_norm')
        }

    return (split_sizes, results_dict)


def create_summary(run_folderpath):
    split_sizes, results_dict = summarize_runs(run_folderpath)

    fpath = os.path.join(run_folderpath, 'summary.pickle')

    with open(fpath, 'wb') as out:
        output = (split_sizes, results_dict)
        pickle.dump(output, out)


# create summary for all run folders in the folderpath
def create_all_summaries(folderpath):

    for fname in os.listdir(folderpath):
        fpath = os.path.join(folderpath, fname)

        if os.path.isdir(fpath):
            print fname
            create_summary(fpath)


def load_summary(run_folderpath):

    fpath = os.path.join(run_folderpath, 'summary.pickle')

    with open(fpath, 'rb') as f:
        return pickle.load(f)


