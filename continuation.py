
import numpy as np


# two step iterator
class regular_continuation:
    def __init__(self, minimum_atom_function, start_const, nconsts, eps):
        self.minimum_atom_function = minimum_atom_function
        self.start_const = start_const
        self.nconsts = nconsts
        self.eps = eps
        # mutable part
        self.n = 0
        self.gamma = start_const

    def is_empty(self):
        return self.n >= self.nconsts

    def next(self, x):
        self.gamma *= self.eps
        self.n += 1

    def get_minimum_atom_function(self):
        return lambda grad_x: self.gamma * self.minimum_atom_function(grad_x)

class seed_continuation:
    def __init__(self, seed_minimum_atom_factory, start_seed, start_const, nconsts, eps, alpha):
        self.seed_minimum_atom_factory = seed_minimum_atom_factory
        self.start_seed = start_seed
        self.start_const = start_const
        self.nconsts = nconsts
        self.eps = eps
        self.alpha = alpha
        # mutable part
        self.n = 0
        self.cur_seed = start_const * start_seed

    def is_empty(self):
        return self.n >= self.nconsts

    def next(self, x):
        max_aligned_cur_seed = self.seed_minimum_atom_factory(self.cur_seed)(-x)

        self.cur_seed = self.eps * (self.alpha * max_aligned_cur_seed + (1.0 - self.alpha) * x)
        self.n += 1

    def get_minimum_atom_function(self):
        return self.seed_minimum_atom_factory(self.cur_seed)


