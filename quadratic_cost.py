
import numpy as np


def quadratic_cost_factory(A, b):
    def quadratic_cost_function(x):
        N = A.shape[0]
        error = A.dot(x) - b
        cost_x  = (1.0 / (2.0 * N)) * np.sum(np.multiply(error, error))
        return cost_x

    return quadratic_cost_function


def quadratic_gradient_factory(A, b):
    def quadratic_gradient_function(x):
        N = A.shape[0]
        error = A.dot(x) - b
        grad_x  = (1.0 / N) * A.T.dot(error)
        return grad_x

    return quadratic_gradient_function

