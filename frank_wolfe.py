
import numpy as np


# TODO
# - implement some form of step tuning
# - implement some more reasonable stopping criteria
# - possibility of having some of these functions be passed as arguments

# types for the functions mentioned in the higher order optimizer
# def cost_function(x)
# def gradient_function(x)
# def minimum_atom_function(grad_x)

def frank_wolfe_optimizer(x0, cost_function, gradient_function,
    minimum_atom_function, max_nsteps, max_dgap, max_delta):

    def update_function(x, s, alpha):
        return (1.0 - alpha) * x + alpha * s

    def duality_gap_function(grad_x, x, s):
        return np.sum(np.multiply(grad_x, (x - s)))

    def tune_step(x, s):
        beta = 0.11
        alpha = 1.0

        best_alpha = 0.0
        best_cost = np.inf
        while True:
            xtest = update_function(x, s, alpha)
            cost = cost_function(xtest)

            if cost < best_cost:
                best_alpha = alpha
                best_cost = cost
                alpha *= beta
            else:
                break

        return best_alpha

    xk = x0
    k = 0
    while True:
        cost_xk = cost_function(xk)
        grad_xk = gradient_function(xk)
        sk = minimum_atom_function(grad_xk)
        dgap_xk = duality_gap_function(grad_xk, xk, sk)

        alphak = tune_step(xk, sk)
        #alphak = 2.0 / (2.0 + k)
        xk_next = update_function(xk, sk, alphak)
        delta_norm = np.linalg.norm(xk_next - xk)

        #if tol_xk < max_tol or k >= max_nsteps:
        #if k >= max_nsteps:
        # funny stopping criteria
        if k > 10 and ((dgap_xk <= max_dgap and delta_norm <= max_delta) or k >= max_nsteps):
        #if dgap_xk < 1e-8 and k >= max_nsteps:
            break

        #print "step %d, cost %0.5e, duality gap %0.5e,"\
            #" tol %0.5e, step size %0.5e, delta norm %0.5e" % (
            #k, cost_xk, dgap_xk, tol_xk, alphak, delta_norm)

        xk = xk_next
        k += 1

    res = {
        'k' : k,
        'xk' : xk,
        'cost_xk' : cost_xk,
        'dgap_xk' : dgap_xk,
        }

    return res


if __name__ == '__main__':
    import pprint
    np.set_printoptions(threshold=0)

    # simple quadratic problem
    d = 1500
    cost_function = lambda x: 0.5 * np.sum(np.multiply(x, x))
    gradient_function = lambda x: x
    minimum_atom_function = lambda x: - x / np.linalg.norm(x)
    max_nsteps = 100
    max_dgap = 1e-3
    max_delta = 1e-3
    x0 = np.random.randn(d); x0 /= np.linalg.norm(x0)

    res = frank_wolfe_optimizer(x0, cost_function, gradient_function,
        minimum_atom_function, max_nsteps, max_dgap, max_delta)

    pprint.pprint(res)



