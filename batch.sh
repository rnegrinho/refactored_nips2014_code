#!/bin/bash

set -e

## *** common arguments
#usage: run_experiments.py [-h] [--model_rg_init MODEL_RG_INIT]
#                          model_d model_nnz {const,unif,gauss,lap} N_train
#                          N_val N_test noise_std min_samples nsplits out_data
#                          out_text max_nsteps max_dgap max_delta {regular,seed} ...
#
#positional arguments:
#  {regular,seed}
#
#optional arguments:
#  -h, --help            show this help message and exit
#
#model options:
#  model_d               dimension of the model
#  model_nnz             number of nonzeros of the model
#  {const,unif,gauss,lap}
#                        model random generator
#  --model_rg_init MODEL_RG_INIT
#                        initialization of the model random generator
#
#dataset options:
#  N_train               number of training samples
#  N_val                 number of validation samples
#  N_test                number of test samples
#  noise_std             standard deviation of the added gaussian noise
#
#simulation options:
#  min_samples           minimum number of samples to use
#  nsplits               number of splits for which we run the experiments
#  out_data              name for the output pickle file with the data
#  out_text              name for the output text file with a summary of the
#                        experiment
#
#optimization options:
#  max_nsteps            maximum number of steps for the optimizer
#  max_delta               maximum tolerance for the optimization procedure (dgap
#                        / cost)

## *** regular estimation
#usage: run_experiments.py model_d model_nnz {const,unif,gauss,lap} N_train N_val
#                   N_test noise_std min_samples nsplits out_data out_text
#                   max_nsteps max_dgap max_delta regular
#       [-h] {l1,l2} nconsts start_const eps
#
#optional arguments:
#  -h, --help   show this help message and exit
#
#estimation options:
#  {l1,l2}      regularization to use
#  nconsts      number of regularization constants
#  start_const  initial regularization ball size
#  eps          expansion constant of the regularization ball (>1.0)

## *** seed estimation
#usage: run_experiments.py model_d model_nnz {const,unif,gauss,lap} N_train N_val
#                   N_test noise_std min_samples nsplits out_data out_text
#                   max_nsteps max_dgap max_delta seed
#       [-h] [--seed_nnz SEED_NNZ] [--seed_rg {const,unif,gauss,lap}]
#       [--seed_rg_init SEED_RG_INIT]
#       {sym,oct} nconsts start_const eps alpha
#
#optional arguments:
#  -h, --help            show this help message and exit
#
#estimation options:
#  {sym,oct}             regularization to use
#  nconsts               number of regularization constants
#  start_const           initial regularization ball size
#  eps                   expansion constant of the regularization ball (>1.0)
#  alpha                 convex combination of the previous seed and current
#                        solution (v_t := alpha * v_(t-1) + (1 - alpha) * w_t)
#  --seed_nnz SEED_NNZ   number of nonzeros of the seed (if not set, the nnz is
#                        equal to the nnz of the model)
#  --seed_rg {const,unif,gauss,lap}
#                        seed random generator (if not set, the seed is equal
#                        to the model)
#  --seed_rg_init SEED_RG_INIT
#                        initialization of the seed random generator


# some of these arguments may actually be set using some dependence on the 
# values 

# the only thing that can be of use is trying some values for d and nnz
# but probably paired

# NOTE check the values from the previous experiments
# take a look at the information from the nips comments

# min samples can be a fraction of the number of nonzeros, a fraction of the 
# dimension or a multiple of the number of nonzeros, (or just the number of nonzeros)
# which one is lowest really

# NOTE that I have changed the setting of the frank wolfe algorithm for 
# it to work when we have little noise
# this should be better done later

# use multiple of ten to do this
# note use some function of the number of variables to make this easier

# model 
model_d=$1
model_nnz=$2
###### the parameters above may be used in a loop
model_rg=unif
model_rg_init='--model_rg_init 0'

# dataset
#N_train=$(( 2*$model_d < 20*$model_nnz ? 2*$model_d : 20*$model_nnz ))
N_train=$((2 * $model_d))
N_val=500
N_test=500
noise_std=$3
# for reproducibility purposes
dataset_rg_init='--dataset_rg_init 1${i}'
noise_rg_init='--noise_rg_init 2${i}'

# simulation (NOTE, single split for now)
#min_samples=$(( $model_nnz/10 > 10 ? $model_nnz/10 : 10 ))
min_samples=10
nsplits=10

# optimization
max_nsteps=1000
max_dgap=1e-5
max_delta=1e-4

# estimation 
start_const=1e-3
nconsts=20
eps=2

# runs
nruns=10
out_folder="out_final_fixed_noise"

i=0
while [ $i -lt $nruns ]; do

    echo "run $i"
    out_common="d${model_d}_nnz${model_nnz}_${model_rg}_noise${noise_std}"

    out_path="${out_folder}/l1_${out_common}"; mkdir -p $out_path
    eval ipython run_experiments.py -- $model_d $model_nnz $model_rg $model_rg_init \
        $N_train $N_val $N_test $noise_std $dataset_rg_init $noise_rg_init \
        $min_samples $nsplits ${out_path}/run${i}_data ${out_path}/run${i}_info \
        $max_nsteps $max_dgap $max_delta \
        regular l1 $nconsts $start_const $eps #> \
        #${out_path}/run${i}_log.txt &

    out_path="${out_folder}/l2_${out_common}"; mkdir -p $out_path
    eval ipython run_experiments.py -- $model_d $model_nnz $model_rg $model_rg_init \
        $N_train $N_val $N_test $noise_std $dataset_rg_init $noise_rg_init \
        $min_samples $nsplits ${out_path}/run${i}_data ${out_path}/run${i}_info \
        $max_nsteps $max_dgap $max_delta \
        regular l2 $nconsts $start_const $eps #> \
        #${out_path}/run${i}_log.txt &

    alpha=1.0
    out_path="${out_folder}/sym_${out_common}"; mkdir -p $out_path
    eval ipython run_experiments.py -- $model_d $model_nnz $model_rg $model_rg_init \
        $N_train $N_val $N_test $noise_std $dataset_rg_init $noise_rg_init \
        $min_samples $nsplits ${out_path}/run${i}_data ${out_path}/run${i}_info \
        $max_nsteps $max_dgap $max_delta \
        seed sym $nconsts $start_const $eps $alpha #> \
        #${out_path}/run${i}_log.txt &

    out_path="${out_folder}/oct_${out_common}"; mkdir -p $out_path
    eval ipython run_experiments.py -- $model_d $model_nnz $model_rg $model_rg_init \
        $N_train $N_val $N_test $noise_std $dataset_rg_init $noise_rg_init \
        $min_samples $nsplits ${out_path}/run${i}_data ${out_path}/run${i}_info \
        $max_nsteps $max_dgap $max_delta \
        seed oct $nconsts $start_const $eps $alpha #> \
        #${out_path}/run${i}_log.txt &

    #### these plots are interesting but the ones of figure 4 don't require these data
    #alpha=0.0
    #out_path="${out_folder}/sym_cont_${out_common}"; mkdir -p $out_path
    #eval ipython run_experiments.py -- $model_d $model_nnz $model_rg $model_rg_init \
        #$N_train $N_val $N_test $noise_std $dataset_rg_init $noise_rg_init \
        #$min_samples $nsplits ${out_path}/run${i}_data ${out_path}/run${i}_info \
        #$max_nsteps $max_dgap $max_delta \
        #seed sym $nconsts $start_const $eps $alpha --seed_rg $model_rg --seed_nnz $model_d #> \
        ##${out_path}/run${i}_log.txt 

    #out_path="${out_folder}/oct_cont_${out_common}"; mkdir -p $out_path
    #eval ipython run_experiments.py -- $model_d $model_nnz $model_rg $model_rg_init \
        #$N_train $N_val $N_test $noise_std $dataset_rg_init $noise_rg_init \
        #$min_samples $nsplits ${out_path}/run${i}_data ${out_path}/run${i}_info \
        #$max_nsteps $max_dgap $max_delta \
        #seed oct $nconsts $start_const $eps $alpha --seed_rg $model_rg --seed_nnz $model_d #> \
        ##${out_path}/run${i}_log.txt &

    # with a just a single cost, just to trace the regularization path for 
    # several splits
    #alpha=1.0
    #single_start_const=1.0
    #single_nconsts=1
    #out_path="${out_folder}/sym_single_const_${out_common}"; mkdir -p $out_path
    #eval ipython run_experiments.py -- $model_d $model_nnz $model_rg $model_rg_init \
        #$N_train $N_val $N_test $noise_std $dataset_rg_init $noise_rg_init \
        #$min_samples $nsplits ${out_path}/run${i}_data ${out_path}/run${i}_info \
        #$max_nsteps $max_dgap $max_delta \
        #seed sym $single_nconsts $single_start_const $eps $alpha #> \
        ##${out_path}/run${i}_log.txt &

    #out_path="${out_folder}/oct_single_const_${out_common}"; mkdir -p $out_path
    #eval ipython run_experiments.py -- $model_d $model_nnz $model_rg $model_rg_init \
        #$N_train $N_val $N_test $noise_std $dataset_rg_init $noise_rg_init \
        #$min_samples $nsplits ${out_path}/run${i}_data ${out_path}/run${i}_info \
        #$max_nsteps $max_dgap $max_delta \
        #seed oct $single_nconsts $single_start_const $eps $alpha #> \
        ##${out_path}/run${i}_log.txt &

    let i=i+1
done

